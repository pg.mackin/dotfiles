{:user {:plugins [[jonase/eastwood "0.2.1"]
                  [cider/cider-nrepl "0.9.1"]
                  [venantius/ultra "0.3.4"]
                  ]
        :dependencies [[jonase/eastwood "0.2.1" :exclusions [org.clojure/clojure]]]
        :ultra {:color-scheme :solarized_dark}}
        }
 }
