alias j=jobs
alias ll='ls -laF'
alias "apt-get"='apt-get -y'
alias "apt install"="apt install -y"
alias ls='ls -F'

gdiff() { git log -p --reverse origin/master..origin/$1 }
gbrowse() { hub browse `git config remote.origin.url|gawk 'match($0, /:(\w+)/, arr) { print arr[1]}'`/`git symbolic-ref --short HEAD`/$1 }

alias pym='python manage.py'
alias sp='python manage.py shell_plus'
alias cs='python manage.py collectstatic --noinput'
alias rgrep='grep -r'
alias sd='sudo'

alias 'dev!'='source dev'

alias ap='ansible-playbook'
alias al='ansible-lint'

alias showpkg-files='dpkg-query -L'

mm() { ./manage.py makemigrations $1 && ./manage.py migrate }
